﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DBSourceCtrl
{ 
    public class ViewConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            byte view = (byte)value;
            var temp = view - 1;
            return (byte)temp;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
           return (ViewUAV)System.Convert.ToByte((int)value+1);
        }
    }
}
