﻿
using DBSourceCtrl.Models;
using System.ComponentModel;
using System.Windows.Controls.WpfPropertyGrid;


namespace DBSourceCtrl
{
    [CategoryOrder("Common", 1)]
    public class LocalProperties
    {
        public event PropertyChangedEventHandler OnPropertyChanged = (obg, str) => { };

        public LocalProperties()
        {
            General = new PropertyTemplate();

            General.PropertyChanged += PropertyChanged;
        }

        private void PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            OnPropertyChanged(this, e);
        }


        [Category("Common")]
        [DisplayName(" ")]
        //[NotifyParentProperty(true)]
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        public PropertyTemplate General { get; set; }


        #region Model Methods

        public LocalProperties Clone()
        {
            return new LocalProperties
            {
                General = General.Clone()
            };
        }

        public void Update(LocalProperties localProperties)
        {
            General.Update(localProperties.General);
        }

        public bool EqualTo(LocalProperties localProperties)
        {
            return General.EqualTo(localProperties.General);
        }

        #endregion
    }
}
