﻿
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ItemFreqCtrl.Models
{
    public class FreqWork : INotifyPropertyChanged
    {
        private double _frequencyMin = 0;

        [NotifyParentProperty(true)]
        [DisplayName(nameof(FrequencyMin))]
        [Category(nameof(FreqWork))]
        public double FrequencyMin
        {
            get { return _frequencyMin; }
            set
            {
                if (_frequencyMin != value)
                {
                    _frequencyMin = value;
                    OnPropertyChanged();

                }
            }
        }


        private double _frequencyMax = 0;
        [NotifyParentProperty(true)]
        [DisplayName(nameof(FrequencyMax))]
        [Category(nameof(FreqWork))]
        public double FrequencyMax
        {
            get { return _frequencyMax; }
            set
            {
                if (_frequencyMax != value)
                {
                    _frequencyMax = value;
                    OnPropertyChanged();

                }
            }
        }

        private float _band = 0;
        [NotifyParentProperty(true)]
        [DisplayName(nameof(Band))]
        [Category(nameof(FreqWork))]
        public float Band
        {
            get { return _band; }
            set
            {
                if (_band != value)
                {
                    _band = value;
                    OnPropertyChanged();

                }
            }
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
    }
}
