﻿using ItemFreqCtrl.Models;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Linq;
using System.Windows.Media;
using System.Windows.Controls.WpfPropertyGrid;

namespace DBSourceCtrl.Models
{
    //[Category("Hello")]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class PropertyTemplate : INotifyPropertyChanged
    {

        private int _id;
        [NotifyParentProperty(true)]
        public int Id
        {
            get => _id;
            set
            {
                if (_id == value) return;
                _id = value;
                OnPropertyChanged();
            }
        }


        private string _name = "";
        [NotifyParentProperty(true)]
        [DisplayName(nameof(Name))]
        [Category(nameof(PropertyTemplate))]
        public string Name
        {
            get => _name;
            set
            {
                if (_name == value) return;
                _name = value;
                OnPropertyChanged();
            }
        }


        private ViewUAV _view = ViewUAV.Multicopter;
        [NotifyParentProperty(true)]
        [DisplayName(nameof(View))]
        [Category(nameof(PropertyTemplate))]
        public ViewUAV View
        {
            get { return _view; }
            set
            {
                if (_view == value) return;
                _view = value;
                OnPropertyChanged();
            }
        }

        private ImageSource _image = null ;
        [NotifyParentProperty(true)]
        [DisplayName(nameof(Image))]
        [Category(nameof(PropertyTemplate))]
        public ImageSource Image
        {
            get => _image;
            set
            {
                if (_image == value) return;
                _image = value;
                OnPropertyChanged();
            }
        }


        //private string _imagePath = "";
        //[NotifyParentProperty(true)]
        //public string ImagePath
        //{
        //    get => _imagePath;
        //    set
        //    {
        //        if (_imagePath == value) return;
        //        _imagePath = value;

        //        if (_imagePath != null && _imagePath != String.Empty)
        //        {
        //            var converter = new ImageSourceConverter();
        //            Image = (ImageSource)converter.ConvertFromString(_imagePath);
        //        }
        //         //= System.Drawing.Image.FromFile(_imagePath);
        //        OnPropertyChanged();
        //    }
        //}

        private ObservableCollection<FreqWork> _freqWork = new ObservableCollection<FreqWork>() { new FreqWork() {} };
        [NotifyParentProperty(true)]
        [DisplayName(" ")]
        [Category(nameof(PropertyTemplate))]
        [TypeConverter(typeof(ExpandableObjectConverter))]
        public ObservableCollection<FreqWork> FreqWork
        {
            get { return _freqWork; }
            set
            {
                if (_freqWork != value)
                {
                    _freqWork = value;
                    OnPropertyChanged();
                }
            }
        }


        


        #region IModelMethods

        public PropertyTemplate Clone()
        {
            return new PropertyTemplate
            {
                Id = Id,
                Name = Name,
                View = View,
                //ImagePath = ImagePath,
                Image = Image,
                FreqWork = new ObservableCollection<FreqWork>(FreqWork)
            };
        }

        public bool EqualTo(PropertyTemplate model)
        {
            return Id == model.Id
                && Name == model.Name
                && View == model.View
                //&& ImagePath == model.ImagePath
                && Image == model.Image
                && CheckFreqWork(FreqWork, model.FreqWork);
        }

        private bool CheckFreqWork(ObservableCollection<FreqWork> col1, ObservableCollection<FreqWork> col2)
        {
            if (col1.Count != col2.Count)
                return false;

            var properties = typeof(FreqWork).GetProperties();
            for (var i = 0; i < col1.Count(); i++)
            {
                foreach (var property in properties)
                {
                    var value1 = property.GetValue(col1[i]);
                    var value2 = property.GetValue(col2[i]);


                    if (!Equals(value1, value2))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public void Update(PropertyTemplate model)
        {
            Name = model.Name;
            View = model.View;
            //ImagePath = model.ImagePath;
            Image = model.Image;
            FreqWork = new ObservableCollection<FreqWork>( model.FreqWork);
        }
        #endregion


        public PropertyTemplate()
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
        }


        public PropertyTemplate(TemplateSource recTemplate)
        {
            Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
            Id = recTemplate.ID;
            Name = recTemplate.Name;
            View = recTemplate.View;
            //            Intent intent = new Intent(CallingActivity.this, CalledActivity.class);
            //intent.putExtra("bitmap", bitmap);
            //startActivity(intent);
            Image = recTemplate.Image;
            
            //ImagePath = ((BitmapImage)recTemplate.Image).UriSource.ToString();
            FreqWork = new ObservableCollection<FreqWork>(recTemplate.FreqWork);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));

        }

    }
}
