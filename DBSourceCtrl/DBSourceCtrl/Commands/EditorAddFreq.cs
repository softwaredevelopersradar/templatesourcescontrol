﻿using System.Collections.ObjectModel;
using ItemFreqCtrl.Models;

namespace DBSourceCtrl
{
    public class EditorAddFreq
    {
        private static RelayCommand executeCommand;

        public static RelayCommand ExecuteCommand
        {
            get
            {
                return executeCommand ??
                    (executeCommand = new RelayCommand(AddItem, CanExecute));
            }
            set
            {
                if (executeCommand == value) return;
                executeCommand = value;
            }
        }


        public static void AddItem(object e)
        {
            (e as ObservableCollection<FreqWork>).Add(new FreqWork());
            return;
        }

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}
