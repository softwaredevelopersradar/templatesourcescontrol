﻿using System.Windows.Controls;
using System.Windows.Media;

namespace DBSourceCtrl
{
    public class EditorPNGFileCommands
    {
        private static RelayCommand executeCommand;

        public static RelayCommand ExecuteCommand
        {
            get
            {
                return executeCommand ??
                    (executeCommand = new RelayCommand(ShowDialog, CanExecute));
            }
            set
            {
                if (executeCommand == value) return;
                executeCommand = value;
            }
        }


        public static void ShowDialog(object e)
        {
            System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog()
            {
                Filter = "Image files(*.png)|*.png"
            };

            //if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            //{
            //    (e as TextBox).Text = ofd.FileName;
            //    return;
            //}

            if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                var converter = new ImageSourceConverter();
                (e as Image).Source = (ImageSource)converter.ConvertFromString(ofd.FileName);
                return;
            }

        }

        public static bool CanExecute(object e)
        {
            return true;
        }
    }
}
