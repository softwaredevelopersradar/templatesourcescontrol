﻿using System.Windows.Input;
using System.Windows.Data;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace DBSourceCtrl
{
    public static class TextBoxExtend
    {
        #region OnTyping

        public static readonly DependencyProperty CommitOnIntTypingProperty = DependencyProperty.RegisterAttached("CommitOnIntTyping", typeof(bool), typeof(TextBoxExtend), new FrameworkPropertyMetadata(false, OnCommitOnIntTypingChanged));

        private static void OnCommitOnIntTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxCommitValueWhileTyping;
                textbox.PreviewTextInput -= TextboxCommitPreviewTextInput;
                textbox.PreviewKeyDown -= Textbox_PreviewKeyDown;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxCommitPreviewTextInput;
                textbox.KeyUp += TextBoxCommitValueWhileTyping;
                textbox.PreviewKeyDown += Textbox_PreviewKeyDown;
            }
        }


        private static void TextboxCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "-0123456789".IndexOf(e.Text) < 0;
        }

        static void TextBoxCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Escape)
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            if (e.Key == Key.Back && textbox.Text == "-")
                return;

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        public static void SetCommitOnIntTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnIntTypingProperty, value);
        }

        public static bool GetCommitOnIntTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnIntTypingProperty);
        }
        #endregion

        #region OnTyping Float

        public static readonly DependencyProperty CommitOnFloatTypingProperty = DependencyProperty.RegisterAttached("CommitOnFloatTyping", typeof(bool), typeof(TextBoxExtend), new FrameworkPropertyMetadata(false, OnCommitOnFloatTypingChanged));

        private static void OnCommitOnFloatTypingChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.PreviewTextInput -= TextboxFloatCommitPreviewTextInput;
                textbox.KeyUp -= TextBoxFloatCommitValueWhileTyping;
                //textbox.PreviewKeyDown -= Textbox_PreviewKeyDown;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxFloatCommitPreviewTextInput;
                textbox.KeyUp += TextBoxFloatCommitValueWhileTyping;
                //textbox.PreviewKeyDown += Textbox_PreviewKeyDown;
            }
        }

        static void TextBoxFloatCommitValueWhileTyping(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.OemMinus || e.Key == Key.Escape)
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            string text = textbox.Text.Remove(textbox.Text.Length - 1, 1);
            var charView = GetCharFromKey(e.Key).ToString();

            if ((charView == "." || charView == ",") && !(text.Contains(",") || text.Contains("."))) return;

            if (charView != "." && charView != "," && !int.TryParse(charView, out var result) && e.Key != Key.Back)
                return;

            if (e.Key == Key.Back && textbox.Text == "-")
                return;

            if (textbox.Text.Contains(",") || textbox.Text.Contains("."))
            {

                textbox.Text = SubstituteSeparator(textbox.Text);

                textbox.SelectionStart = textbox.Text.Length;
                textbox.SelectionLength = 0;

                if (e.Key == Key.D0 || e.Key == Key.NumPad0)
                    return;
            }

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null)
            {
                expression.UpdateSource();
                expression.UpdateTarget();
            }
            e.Handled = true;
        }

        private static string SubstituteSeparator(string text)
        {
            char separator = System.Globalization.CultureInfo.CurrentCulture.NumberFormat.CurrencyDecimalSeparator[0];
            string Source = text.Replace(',', separator);
            Source = Source.Replace('.', separator);
            Source = RemoveAdditionalSepparators(Source, separator);
            return Source;
        }

        private static string RemoveAdditionalSepparators(string text, char separator)
        {
            int index = text.IndexOf(separator);
            text = text.Trim(separator);
            if (!text.Contains(separator))
                text = text.Insert(index, separator.ToString());
            return text;
        }


        private static void TextboxFloatCommitPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "-0123456789.,".IndexOf(e.Text) < 0;
        }


        public static void SetCommitOnFloatTyping(TextBox target, bool value)
        {
            target.SetValue(CommitOnFloatTypingProperty, value);
        }

        public static bool GetCommitOnFloatTyping(TextBox target)
        {
            return (bool)target.GetValue(CommitOnFloatTypingProperty);
        }
        #endregion

        #region
        public enum MapType : uint
        {
            MAPVK_VK_TO_VSC = 0x0,
            MAPVK_VSC_TO_VK = 0x1,
            MAPVK_VK_TO_CHAR = 0x2,
            MAPVK_VSC_TO_VK_EX = 0x3,
        }

        [DllImport("user32.dll")]
        public static extern int ToUnicode(
            uint wVirtKey,
            uint wScanCode,
            byte[] lpKeyState,
            [Out, MarshalAs(UnmanagedType.LPWStr, SizeParamIndex = 4)]
            StringBuilder pwszBuff,
            int cchBuff,
            uint wFlags);

        [DllImport("user32.dll")]
        public static extern bool GetKeyboardState(byte[] lpKeyState);

        [DllImport("user32.dll")]
        public static extern uint MapVirtualKey(uint uCode, MapType uMapType);

        public static char GetCharFromKey(Key key)
        {
            char ch = ' ';

            int virtualKey = KeyInterop.VirtualKeyFromKey(key);
            byte[] keyboardState = new byte[256];
            GetKeyboardState(keyboardState);

            uint scanCode = MapVirtualKey((uint)virtualKey, MapType.MAPVK_VK_TO_VSC);
            StringBuilder stringBuilder = new StringBuilder(2);

            int result = ToUnicode((uint)virtualKey, scanCode, keyboardState, stringBuilder, stringBuilder.Capacity, 0);
            switch (result)
            {
                case -1:
                    break;
                case 0:
                    break;
                case 1:
                    {
                        ch = stringBuilder[0];
                        break;
                    }
                default:
                    {
                        ch = stringBuilder[0];
                        break;
                    }
            }
            return ch;
        }
        #endregion

        #region CommitOnIP
        public static readonly DependencyProperty CommitOnIPProperty = DependencyProperty.RegisterAttached("CommitOnIP",
            typeof(bool), typeof(TextBoxExtend), new FrameworkPropertyMetadata(false, OnCommitOnIPChanged));

        private static void OnCommitOnIPChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            var textbox = sender as TextBox;
            if (textbox == null) return;

            var wasBound = (bool)(e.OldValue);
            var needToBind = (bool)(e.NewValue);

            if (wasBound)
            {
                textbox.KeyUp -= TextBoxCommitOnIpValue;
                textbox.PreviewTextInput -= TextboxCommitOnIpPreviewInput;
            }

            if (needToBind)
            {
                textbox.PreviewTextInput += TextboxCommitOnIpPreviewInput;
                textbox.KeyUp += TextBoxCommitOnIpValue;
            }
        }

        private static void Textbox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Tab)
            {
                var textbox = sender as TextBox;
                var parent = textbox.Parent;
                if (parent == null)
                    return;
                foreach (var final in (parent as Grid).Children)
                {
                    if (final.Equals(sender)) continue;

                    if (!(final is TextBox)) continue;
                    if ((final as TextBox).IsEnabled && (final as TextBox).Focusable && !(final as TextBox).IsReadOnly)
                    {
                        (final as UIElement).Focus();
                        e.Handled = true;
                        return;
                    }
                }
                (sender as UIElement).Focus();
                e.Handled = true;
            }

        }

        private static void TextboxCommitOnIpPreviewInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = "0123456789.".IndexOf(e.Text) < 0;
        }

        static void TextBoxCommitOnIpValue(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape || e.Key == Key.Tab) //e.Key == Key.Back ||
                return;

            var textbox = sender as TextBox;

            if (textbox == null) return;

            if (textbox.Text == "") return;

            BindingExpression expression = textbox.GetBindingExpression(TextBox.TextProperty);
            if (expression != null) expression.UpdateSource();
            e.Handled = true;
        }

        public static void SetCommitOnIP(TextBox target, bool value)
        {
            target.SetValue(CommitOnIPProperty, value);
        }

        public static bool GetCommitOnIP(TextBox target)
        {
            return (bool)target.GetValue(CommitOnIPProperty);
        }
        #endregion}
    }
}
