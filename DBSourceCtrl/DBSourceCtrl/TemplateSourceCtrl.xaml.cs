﻿using DBSourceCtrl.Models;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;


namespace DBSourceCtrl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class TemplateUAVCtrl : UserControl, INotifyPropertyChanged
    {
        private ObservableCollection<TemplateSource> _templateSources = new ObservableCollection<TemplateSource>();
        public ObservableCollection<TemplateSource> TemplateSources
        {
            get { return _templateSources; }
            set
            {
                if (_templateSources != value)
                {
                    _templateSources = value;
                    OnPropertyChanged();

                }
            }
        }


        private TemplateSource _droneSelected;
        public TemplateSource DroneSelected
        {
            get
            {
                return _droneSelected;
            }
            set
            {
                _droneSelected = value;
                OnPropertyChanged();
            }
        }

        public DllGrozaSProperties.Models.Languages CLanguage
        {
            get { return (DllGrozaSProperties.Models.Languages)GetValue(CLanguageProperty); }
            set { SetValue(CLanguageProperty, value); Translator.LoadDictionary(value); }
        }

        public static readonly DependencyProperty CLanguageProperty =
            DependencyProperty.Register("CLanguage", typeof(DllGrozaSProperties.Models.Languages),
                typeof(TemplateUAVCtrl));

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

        public TemplateUAVCtrl()
        {
            InitializeComponent();

            DataContext = this;
            
        }


        private void UpdateListItem()
        {
            try
            {
                
                Dispatcher.BeginInvoke(new Action(() =>
                {
                   
                }), DispatcherPriority.Background);
            }
            catch
            { }
        }

        public DronePropertyView dronePropertyWindow;

        public event EventHandler<DronePropertyView> OnIsWindowPropertyOpen;
        public event EventHandler<PropertyTemplate> OnAddRecord;
        public event EventHandler<PropertyTemplate> OnChangeRecord;
        public event EventHandler<PropertyTemplate> OnDeleteRecord;
        public event EventHandler OnClearRecords;
        public event EventHandler OnLoadDefaultRecords;
        private void AddSourceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                dronePropertyWindow = new DronePropertyView();

                OnIsWindowPropertyOpen?.Invoke(this, dronePropertyWindow);

                if (dronePropertyWindow.ShowDialog() == true)
                {
                    // Событие добавления одной записи
                    OnAddRecord?.Invoke(this, dronePropertyWindow.DroneProperties.General);
                }
            }
            catch (Exception ex)
            { }
        }

        private void UpdateSourceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DroneSelected != null)
                {
                    dronePropertyWindow = new DronePropertyView(DroneSelected);

                    OnIsWindowPropertyOpen?.Invoke(this, dronePropertyWindow);

                    if (dronePropertyWindow.ShowDialog() == true && !dronePropertyWindow.DroneProperties.General.EqualTo(new PropertyTemplate(DroneSelected)))
                    {
                        // Событие изменения одной записи
                        OnChangeRecord?.Invoke(this, dronePropertyWindow.DroneProperties.General);
                    }
                }
            }
            catch(Exception ex) { }
        }

        private void RemoveSourceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (DroneSelected != null)
                {
                    var selectedDroneProperty = new PropertyTemplate() { Id = DroneSelected.ID };

                    // Событие удаления одной записи
                    OnDeleteRecord?.Invoke(this, selectedDroneProperty);
                }
            }
            catch (SystemException ex)
            {
                int f = 8;
            }
        }

        private void ClearSourceButton_Click(object sender, RoutedEventArgs e)
        {
            // Событие удаления всех записей
            OnClearRecords?.Invoke(this, null);
        }

        private void DefaultSourceButton_Click(object sender, RoutedEventArgs e)
        {
            OnLoadDefaultRecords?.Invoke(this, new EventArgs());
        }
    }
}
