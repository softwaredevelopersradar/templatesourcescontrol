﻿namespace DBSourceCtrl
{
    public enum ViewUAV : byte
    {
        Multicopter = 1,
        Plane,
        Static
    }
}