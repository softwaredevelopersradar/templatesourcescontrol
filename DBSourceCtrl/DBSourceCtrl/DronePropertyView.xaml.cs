﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls.WpfPropertyGrid;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using DBSourceCtrl.Models;
using ItemFreqCtrl.Models;

namespace DBSourceCtrl
{
    /// <summary>
    /// Логика взаимодействия для DronePropertyView.xaml
    /// </summary>

    public partial class DronePropertyView : Window
    {
        //[TypeConverter(typeof(ExpandableObjectConverter))]
        //[DisplayName(" ")]
        //[Category(nameof(DroneProperties))]
        public LocalProperties DroneProperties { get; private set; }
        public PropertyTemplate General { get; set; }
        //private DronePropertyViewModel PropertiesViewModel = new DronePropertyViewModel();

        public DronePropertyView()
        {
            InitializeComponent();


            General = new PropertyTemplate() { Name = "-" };
            DroneProperties = new LocalProperties() { General = new PropertyTemplate() { Name = "-" } };
            propertyGrid.SelectedObject = DroneProperties;
            //DroneProperties.PropertyChanged += PropertyChanged;
            Title = SMeaning.meaningAddRecord;
            Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/AddRec.ico", UriKind.Absolute));
            InitEditors();
            //InitProperty();
        }


        public DronePropertyView(TemplateSource recDrone)
        {
            InitializeComponent();

            General = new PropertyTemplate(recDrone);
            DroneProperties = new LocalProperties() { General = new PropertyTemplate(recDrone) { } };
            propertyGrid.SelectedObject = DroneProperties;
            //DroneProperties.PropertyChanged += PropertyChanged;
            Title = SMeaning.meaningChangeRecord;
            Icon = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/ChangeRec.ico", UriKind.Absolute));
            InitEditors();
            InitProperty();
        }


        private void InitEditors()
        {
            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(General), this.GetType(), General.GetType())); //хз про 2й параметр
            propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties.General), DroneProperties.GetType(), DroneProperties.General.GetType())); //хз про 2й параметр


            //propertyGrid.Editors.Add(new PropertiesEditor(nameof(LocalProperties.General), typeof(LocalProperties), typeof(PropertyTemplate))); //хз про 2й параметр
            ////propertyGrid.Editors.Add(new PropertiesEditor(nameof(DroneProperties), typeof(DronePropertiesModel)));
        }

        public void SetLanguagePropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            LoadTranslatorPropertyGrid(language);
            Translator.ChangeLanguagePropertyGrid(language, propertyGrid);
        }

        private void LoadTranslatorPropertyGrid(DllGrozaSProperties.Models.Languages language)
        {
            ResourceDictionary dict = new ResourceDictionary();
            try
            {
                switch (language)
                {
                    case DllGrozaSProperties.Models.Languages.EN:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.EN.xaml",
                                      UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.RU:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.RU.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.AZ:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.AZ.xaml",
                                            UriKind.Relative);
                        break;
                    case DllGrozaSProperties.Models.Languages.SR:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.SRB.xaml",
                            UriKind.Relative);
                        break;
                    default:
                        dict.Source = new Uri("/GrozaS_AWS;component/Languages/UITemplates/StringResource.RU.xaml",
                                          UriKind.Relative);
                        break;
                }

                this.Resources.MergedDictionaries.Add(dict);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
            }
        }


        #region PropertyException

        private void InitProperty()
        {
            foreach (var property in propertyGrid.Properties)
            {
                try
                {
                    if (property.IsBrowsable == false) { continue; }

                    if (property.PropertyValue.SubProperties.Count != 0)
                    {
                        foreach (var subProperty in property.PropertyValue.SubProperties)
                            subProperty.PropertyValue.PropertyValueException += PropertyGridSubException;
                        continue;
                    }
                    property.PropertyValue.PropertyValueException += PropertyGridException;
                }
                catch (Exception ex)
                { MessageBox.Show(ex.Message); }
            }
        }
        
        private void PropertyGridSubException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.ParentValue.ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }

        private void PropertyGridException(object sender, ValueExceptionEventArgs e)
        {
            propertyGrid.Properties[(sender as PropertyItemValue).ParentProperty.Name].SetValue((sender as PropertyItemValue).Value);
        }
        #endregion

       

        private void ButtonApply_Click(object sender, RoutedEventArgs e)
        {
            if (IsAddClick((LocalProperties)propertyGrid.SelectedObject) != null)
            {
                DialogResult = true;
            }
        }

        public LocalProperties IsAddClick(LocalProperties PropertiesWindow)
        {
            List<FreqWork> listToDelete = new List<FreqWork>();
            foreach (FreqWork frq in PropertiesWindow.General.FreqWork)
            {
                if (frq.FrequencyMin == 0 || frq.Band == 0)
                {
                    listToDelete.Add(frq);
                }
            }

            foreach (FreqWork frq in listToDelete)
            {
                PropertiesWindow.General.FreqWork.Remove(frq);
            }
            
            return PropertiesWindow;
        }

        private void gridProperty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (IsAddClick((LocalProperties)propertyGrid.SelectedObject) != null)
                {
                    DialogResult = true;
                }
            }
        }
    }
}
