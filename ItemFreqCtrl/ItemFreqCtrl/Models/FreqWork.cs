﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ItemFreqCtrl.Models
{
    public class FreqWork : INotifyPropertyChanged
    {
        private double _frequency;
        public double Frequency
        {
            get { return _frequency; }
            set
            {
                if (_frequency != value)
                {
                    _frequency = value;
                    OnPropertyChanged();

                }
            }
        }

        private float _band;
        public float Band
        {
            get { return _band; }
            set
            {
                if (_band != value)
                {
                    _band = value;
                    OnPropertyChanged();

                }
            }
        }

        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion
    }
}
