﻿using ItemFreqCtrl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ItemFreqCtrl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ItemFreqCtrl : UserControl, INotifyPropertyChanged
    {
        private ObservableCollection<FreqWork> _freqWork = new ObservableCollection<FreqWork>();
        public ObservableCollection<FreqWork> FreqWork
        {
            get { return _freqWork; }
            set
            {
                if (_freqWork != value)
                {
                    _freqWork = value;
                    OnPropertyChanged();

                }
            }
        }
        public ItemFreqCtrl()
        {
            InitializeComponent();
            DataContext = this;
        }


        #region PropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged([CallerMemberName]string prop = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(prop));
        }


        #endregion

    }

}
