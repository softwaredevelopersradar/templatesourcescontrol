﻿using ItemFreqCtrl.Models;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace ItemFreqCtrl
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ItemFreqCtrl : UserControl
    {
        #region Frequency
        public static readonly DependencyProperty FrequencyProperty = DependencyProperty.Register("Frequency", typeof(double), typeof(ItemFreqCtrl),
                                                                       new PropertyMetadata((double)0, new PropertyChangedCallback(FrequencyChanged)));

        public double Frequency
        {
            get { return (double)GetValue(FrequencyProperty);}
            set { SetValue(FrequencyProperty, value); }
        }

        private static void FrequencyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ItemFreqCtrl itemFreqCtrl = (ItemFreqCtrl)d;

            try
            {
               
            }
            catch
            { }

        }


        #endregion

        #region Band
        public static readonly DependencyProperty BandProperty = DependencyProperty.Register("Band", typeof(float), typeof(ItemFreqCtrl),
                                                                       new PropertyMetadata((float)0, new PropertyChangedCallback(BandChanged)));

        public float Band
        {
            get { return (float)GetValue(BandProperty); }
            set { SetValue(BandProperty, value); }
        }

        private static void BandChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            ItemFreqCtrl itemFreqCtrl = (ItemFreqCtrl)d;

            try
            {

            }
            catch
            { }

        }


        #endregion



        //#region FreqWork
        //public static readonly DependencyProperty FreqWorkProperty = DependencyProperty.Register("FreqWork", typeof(ObservableCollection<FreqWork>), typeof(ItemFreqCtrl),
        //                                                               new PropertyMetadata(new PropertyChangedCallback(FreqWorkChanged)));

        //public ObservableCollection<FreqWork> FreqWork
        //{
        //    get { return (ObservableCollection<FreqWork>)GetValue(FreqWorkProperty); }
        //    set { SetValue(FreqWorkProperty, value); }
        //}

        //private static void FreqWorkChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        //{
        //    ItemFreqCtrl itemFreqCtrl = (ItemFreqCtrl)d;

        //    try
        //    {

        //    }
        //    catch
        //    { }

        //}


        //#endregion
        


    }
}