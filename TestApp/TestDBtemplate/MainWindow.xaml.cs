﻿using DBSourceCtrl.Models;
using ItemFreqCtrl.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestDBtemplate
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = this;


         
            listControl.TemplateSources.Add(
                new TemplateSource()
                {
                    ID = 4,
                    Name = "Da-Vinci",

                    FreqWork = new ObservableCollection<FreqWork>
                    {
                        new FreqWork { FrequencyMin = 5600, FrequencyMax = 5680, Band = 20 },
                        new FreqWork { FrequencyMin = 2400, FrequencyMax = 2460,Band = 10},
                        new FreqWork { FrequencyMin = 1200, FrequencyMax = 1300,Band = 5},
                        new FreqWork { FrequencyMin = 2800, FrequencyMax = 2900,Band = 35},
                        new FreqWork { FrequencyMin = 3600, FrequencyMax = 3650,Band = 50}
                    },
                    Image = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/11_Da-Vinci.png", UriKind.Absolute))

        });
            listControl.TemplateSources.Add(new TemplateSource()
                {
                    ID = 5,
                    Name = "Skylark-1 LE",
                    FreqWork = new ObservableCollection<FreqWork>
                    {
                        new FreqWork {  FrequencyMin = 860, FrequencyMax = 890,Band = 18 }
                        
                    },
                    Image = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/9_Skylark-1 LE.png", UriKind.Absolute))

            }
                );

            listControl.TemplateSources.Add(new TemplateSource()
            {
                ID = 6,
                Name = "Skylark-1 LE",
                FreqWork = new ObservableCollection<FreqWork>
                    {
                        new FreqWork {  FrequencyMin = 860, FrequencyMax = 890,Band = 18 }

                    },
                Image = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/9_Skylark-1 LE.png", UriKind.Absolute))

            }
                );

            listControl.TemplateSources.Add(new TemplateSource()
            {
                ID = 7,
                Name = "Skylark-1 LE",
                FreqWork = new ObservableCollection<FreqWork>
                    {
                        new FreqWork {  FrequencyMin = 860, FrequencyMax = 890,Band = 18 }

                    },
                Image = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/9_Skylark-1 LE.png", UriKind.Absolute))

            }
                );

            listControl.TemplateSources.Add(new TemplateSource()
            {
                ID = 8,
                Name = "Skylark-1 LE",
                FreqWork = new ObservableCollection<FreqWork>
                    {
                        new FreqWork {  FrequencyMin = 860, FrequencyMax = 890,Band = 18 }

                    },
                Image = new BitmapImage(new Uri(@"pack://application:,,,/"
                                    + System.Reflection.Assembly.GetExecutingAssembly().GetName().Name
                                    + ";component/"
                                    + "Images/9_Skylark-1 LE.png", UriKind.Absolute))

            }
                );
        }
    }
}
